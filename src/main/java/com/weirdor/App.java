package com.weirdor;

import com.jfinal.server.undertow.UndertowServer;
import com.weirdor.webrtc.common.config.AppConfig;

public class App {

    public static void main(String[] args) {
        UndertowServer.create(AppConfig.class).start();
    }
}
