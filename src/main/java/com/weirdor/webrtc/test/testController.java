package com.weirdor.webrtc.test;

import com.jfinal.core.Controller;
import com.weirdor.webrtc.ws.ImWsServerStarter;
import org.tio.core.Tio;
import org.tio.server.ServerTioConfig;
import org.tio.websocket.common.WsResponse;

import java.io.IOException;

/**
 * @program: webrtc-server
 * @description:
 * @author: weirdor
 * @create: 2022-10-23 18:59
 **/
public class testController extends Controller {


    public void index() throws IOException {
        System.out.println("test");
        String uid = getPara("uid");
        ServerTioConfig serverTioConfig = ImWsServerStarter.serverTioConfig;
        WsResponse wsResponse = WsResponse.fromText(uid, "utf-8");
        System.out.println(serverTioConfig);
        Tio.sendToAll(serverTioConfig, wsResponse);
        renderText("test");
    }
}
