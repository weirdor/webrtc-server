package com.weirdor.webrtc.ws;

import org.tio.websocket.common.WsResponse;

/**
 * @program: webrtc-server
 * @description:
 * @author: weirdor
 * @create: 2022-10-23 19:20
 **/
public class DemoPacket extends WsResponse {

    /**
     * 为什么定义长度是4？
     * <p>
     * 原因就是java中，一个int占4个字节数
     */
    public static final int HEAD_LENGTH = 4;
    private static final long serialVersionUID = -1L;
    private byte[] body;

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

}