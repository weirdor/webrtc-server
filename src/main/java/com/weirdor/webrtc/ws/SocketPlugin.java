package com.weirdor.webrtc.ws;

import com.jfinal.plugin.IPlugin;

public class SocketPlugin implements IPlugin {

    @Override
    public boolean start() {
        try {
            ImWsServerStarter.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public boolean stop() {
        return false;
    }

}
