package com.weirdor.webrtc.ws;


import org.tio.utils.time.Time;

/**
 * ip数据监控统计，时间段
 *
 * @program: webrtc-server
 * @description:
 * @author: weirdor
 * @create: 2022-10-23 18:46
 **/
public interface IpStatDuration {
    Long DURATION_1 = Time.MINUTE_1 * 5;
    Long[] IPSTAT_DURATIONS = new Long[]{DURATION_1};
}