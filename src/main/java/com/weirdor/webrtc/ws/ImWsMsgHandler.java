package com.weirdor.webrtc.ws;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.log.Log;
import org.tio.core.ChannelContext;
import org.tio.core.Tio;
import org.tio.http.common.HttpRequest;
import org.tio.http.common.HttpResponse;
import org.tio.websocket.common.WsRequest;
import org.tio.websocket.common.WsSessionContext;
import org.tio.websocket.server.handler.IWsMsgHandler;

import java.util.UUID;

/**
 * @program: webrtc-server
 * @description:
 * @author: weirdor
 * @create: 2022-10-23 17:06
 **/
public class ImWsMsgHandler implements IWsMsgHandler {
    public static final ImWsMsgHandler me = new ImWsMsgHandler();
    private static final Log log = Log.getLog(ImWsMsgHandler.class);

    /**
     * 握手时候走的方法
     *
     * @param request
     * @param httpResponse
     * @param channelContext
     * @return
     * @throws Exception
     */
    @Override
    public HttpResponse handshake(HttpRequest request, HttpResponse httpResponse, ChannelContext channelContext) throws Exception {
        String clientip = request.getClientIp();
        String uid = UUID.randomUUID().toString();
        System.out.println("收到来自" + clientip + "的握手包");
        System.out.println("uid:" + uid);
        Tio.bindUser(channelContext, uid);
        return httpResponse;
    }

    /**
     * 握手完成后
     *
     * @param httpRequest
     * @param httpResponse
     * @param channelContext
     * @throws Exception
     */
    @Override
    public void onAfterHandshaked(HttpRequest httpRequest, HttpResponse httpResponse, ChannelContext channelContext) throws Exception {


    }

    /**
     * 字节消息（binaryType = arraybuffer）过来后会走这个方法
     */
    @Override
    public Object onBytes(WsRequest wsRequest, byte[] bytes, ChannelContext channelContext) throws Exception {
        return null;
    }

    /**
     * 链接关闭方法
     *
     * @param wsRequest
     * @param bytes
     * @param channelContext
     * @return
     * @throws Exception
     */
    @Override
    public Object onClose(WsRequest wsRequest, byte[] bytes, ChannelContext channelContext) throws Exception {
        Tio.remove(channelContext, "receive close flag");
        return null;
    }

    /**
     * 接受字符类型消息
     *
     * @param wsRequest
     * @param text
     * @param channelContext
     * @return
     * @throws Exception
     */
    @Override
    public Object onText(WsRequest wsRequest, String text, ChannelContext channelContext) throws Exception {
        WsSessionContext wsSessionContext = (WsSessionContext) channelContext.get();
        //获取websocket握手包
        HttpRequest httpRequest = wsSessionContext.getHandshakeRequest();
        //转换消息
        JSONObject messageInfo = null;
        try {
            messageInfo = JSON.parseObject(text);
        } catch (Exception e) {
//            return Result.fail("消息格式错误，非JSON字符串");
            JSONObject object = new JSONObject();
            object.put("code", 401);
            object.put("msg", "消息格式错误，非JSON字符串");
            return JSON.toJSONString(object);
        }
//        //消息类型
////        Integer type = messageInfo.getInteger("type");
//        //如果是心跳
//        if (type.equals(MessageType.HEARTBEAT_CODE)) {
//            return null;
//        }

        System.out.println("收到消息：" + text);
        return null;
    }


}