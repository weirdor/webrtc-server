package com.weirdor.webrtc.common.config;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.*;
import com.jfinal.json.MixedJsonFactory;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;
import com.weirdor.webrtc.common.factory.logback.LogBackLogFactory;
import com.weirdor.webrtc.test.testController;
import com.weirdor.webrtc.ws.SocketPlugin;

import java.sql.Connection;

/**
 * JFinalClubConfig
 */
public class AppConfig extends JFinalConfig {

    // 使用 jfinal-undertow 时此处仅保留声明，不能有加载代码
    private static Prop p;

    private WallFilter wallFilter;

    /**
     * PropKit.useFirstFound(...) 使用参数中从左到右最先被找到的配置文件
     * 从左到右依次去找配置，找到则立即加载并立即返回，后续配置将被忽略
     */
    static void loadConfig() {
        if (p == null) {
            p = PropKit.useFirstFound("webrtc-config-pro.txt", "webrtc-config-dev.txt");
        }
    }

    /**
     * 抽取成独立的方法，便于 _Generator 中重用该方法，减少代码冗余
     */
    public static DruidPlugin getDruidPlugin() {
        loadConfig();
        return new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password"));
    }

    public void configConstant(Constants me) {
        loadConfig();

        me.setDevMode(p.getBoolean("devMode", true));

        me.setJsonFactory(MixedJsonFactory.me());

        // 开启对 jfinal web 项目组件 Controller、Interceptor、Validator 的注入
        me.setInjectDependency(true);

        // 开启对超类的注入。不开启时可以在超类中通过 Aop.get(...) 进行注入
        me.setInjectSuperClass(true);

        // 设置日志工厂
        me.setLogFactory(new LogBackLogFactory());


    }

    /**
     * 路由
     */
    public void configRoute(Routes me) {
        me.add("/test", testController.class);
    }

    /**
     * 配置模板引擎，通常情况只需配置共享的模板函数
     */
    public void configEngine(Engine me) {

    }

    public void configPlugin(Plugins me) {
        DruidPlugin druidPlugin = getDruidPlugin();
        wallFilter = new WallFilter();            // 加强数据库安全
        wallFilter.setDbType("mysql");
        druidPlugin.addFilter(wallFilter);
        druidPlugin.addFilter(new StatFilter());    // 添加 StatFilter 才会有统计数据
        me.add(druidPlugin);

        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);

        me.add(new SocketPlugin());
    }

    public void configInterceptor(Interceptors me) {

    }

    public void configHandler(Handlers me) {

    }

    /**
     * 本方法会在 jfinal 启动过程完成之后被回调，详见 jfinal 手册
     */
    public void onStart() {
    }
}






