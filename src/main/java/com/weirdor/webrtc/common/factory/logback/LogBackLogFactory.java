package com.weirdor.webrtc.common.factory.logback;


import com.jfinal.log.ILogFactory;
import com.jfinal.log.Log;

/**
 * @author weirdor
 */
public class LogBackLogFactory implements ILogFactory {

    @Override
    public Log getLog(Class<?> clazz) {
        return new LogBackLog(clazz);
    }

    @Override
    public Log getLog(String name) {
        return new LogBackLog(name);
    }

}